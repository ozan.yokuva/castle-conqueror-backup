﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using EC.Managers.Events;

public class LooneyToonsScript : MonoBehaviour
{
    public RectTransform looneyToonsTransition;
    private float _ltTransitionWidth;
    private Tween _currentTransitionTween;

    void Awake()
    {
        _ltTransitionWidth = looneyToonsTransition.sizeDelta.x;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    CloseTheCircle();
        //}

        //if (Input.GetMouseButtonDown(1))
        //{
        //    OpenTheCircle();
        //}
    }

    public void CloseTheCircle(float appearTime, bool finishGame)
    {
        _currentTransitionTween.Kill(false);
        float tempWidth = looneyToonsTransition.sizeDelta.x;
        if (finishGame)
        {
            _currentTransitionTween = DOTween.To(() => tempWidth, x => tempWidth = x, 0f, appearTime)
            .OnUpdate(() => looneyToonsTransition.sizeDelta = new Vector2(tempWidth, looneyToonsTransition.sizeDelta.y)).OnComplete(() => EventRunner.LevelFinish());
        }
        else
        {
            _currentTransitionTween = DOTween.To(() => tempWidth, x => tempWidth = x, 0f, appearTime)
           .OnUpdate(() => looneyToonsTransition.sizeDelta = new Vector2(tempWidth, looneyToonsTransition.sizeDelta.y)).OnComplete(() => EventRunner.LevelRestart());
        }
    }

    public void OpenTheCircle(float disappearTime)
    {
        _currentTransitionTween.Kill(false);
        float tempWidth = looneyToonsTransition.sizeDelta.x;
        _currentTransitionTween = DOTween.To(() => tempWidth, x => tempWidth = x, _ltTransitionWidth, disappearTime)
        .OnUpdate(() => looneyToonsTransition.sizeDelta = new Vector2(tempWidth, looneyToonsTransition.sizeDelta.y));
    }
}
