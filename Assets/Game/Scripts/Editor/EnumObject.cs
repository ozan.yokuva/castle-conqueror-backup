using UnityEngine;
using Sirenix.OdinInspector;
using EC.Editor;

namespace EC.Utility
{
    [CreateAssetMenu(fileName = "Enum List", menuName = "Scriptlable Objects/Enum Object")]
    public class EnumObject : ScriptableObject
    {
        public string enumName;
        public string[] elements;
        
        [Button("Create/Update Enum")]
        public void CreateEnum()
        {
            EnumGenerator.Generate(enumName, elements);
        }
    }
}

