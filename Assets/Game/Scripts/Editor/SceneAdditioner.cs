using UnityEngine.SceneManagement;
using UnityEngine;

namespace EC.Editor
{
    public class SceneAdditioner
    {
        [RuntimeInitializeOnLoadMethod]
        public static void LoadScenes()
        {
            EditorGameSettings gameSettings = Resources.LoadAll<EditorGameSettings>("Managers")[0];
#if UNITY_EDITOR
            if (gameSettings.startFromGameScene)
            {
                SceneManager.LoadScene("EmptyScene");
            }
#endif
        }
    }
}
