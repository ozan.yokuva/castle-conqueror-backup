using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObjects : MonoBehaviour
{
    [Header("Settings")]
    public GameObject point;
    public float speed = 2.0f;
    private Vector3 startPos;
    [Header("Positions")]
    public bool X;
    public bool Y;
    public bool Z;

    void Start()
    {
        startPos = transform.position;
    }

    void Update()
    {
        Vector3 v = startPos;
        float distance;
        if (X == true)
        {
            distance = startPos.x - point.transform.position.x;
            v.x += distance * Mathf.Sin(Time.time * speed);
            transform.position = v;
        }
        if (Y == true)
        {
            distance = startPos.y - point.transform.position.y;
            v.y += distance * Mathf.Sin(Time.time * speed);
            transform.position = v;
        }
        if (Z == true)
        {
            distance = startPos.z - point.transform.position.z;
            v.z += distance * Mathf.Sin(Time.time * speed);
            transform.position = v;
        }


    }
}

