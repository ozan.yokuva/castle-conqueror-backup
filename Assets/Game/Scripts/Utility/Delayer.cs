using UnityEngine;
using UnityEngine.Events;
using System.Threading.Tasks;

namespace EC.Utility
{
	public class Delayer
	{
		public static async void Delay(float duration, UnityAction action)
        {
			await StartDelay(duration, action);
        }
		public static async Task StartDelay(float duration, UnityAction action)
		{
			var end = Time.time + duration;
			while(Time.time < end)
            {
				await Task.Yield();
            }
			action?.Invoke();
		}
	}
}

