﻿using MoreMountains.NiceVibrations;
using System.Collections;
using UnityEngine;
using EC.Managers;

namespace EC.Utility
{
    public class HapticController : MonoSingleton<HapticController>
    {
        private bool vibrate = true;
        public void Vibrate(HapticTypes hapticType = HapticTypes.LightImpact)
        {
            if(MainManager.Instance.SettingsManager.VibOn)
            StartCoroutine(VibrateRoutine(hapticType));
        }
        public IEnumerator VibrateRoutine(HapticTypes hapticType)
        {
            if (vibrate)
            {
                if (MMVibrationManager.HapticsSupported())
                    MMVibrationManager.Haptic(hapticType, false, true, this);
                else
                    MMVibrationManager.Haptic(hapticType, true);
                vibrate = false;
                yield return new WaitForSeconds(.1f);
                vibrate = true;
            }
        }

    }
}

