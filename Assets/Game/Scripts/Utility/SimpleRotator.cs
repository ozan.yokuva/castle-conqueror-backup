using UnityEngine;

namespace EC.Utility
{
    public class SimpleRotator : MonoBehaviour
    {
        public Vector3 rotationSpeed;

        private void Update()
        {
            transform.Rotate(rotationSpeed * Time.deltaTime);
        }
    }
}

