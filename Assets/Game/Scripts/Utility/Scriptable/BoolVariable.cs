using EC.Managers;
using UnityEngine;

[CreateAssetMenu(fileName = "Boolean", menuName = "EC Framework/Create/Variables/Boolean")]
public class BoolVariable : ScriptableVariable
{
    public bool value;

    public override void GetFromRemote()
    {
        value = MainManager.Instance.RemoteConfigManager.GetBoolConfig(name, value);
    }
}
