using UnityEngine.Events;

namespace EC.Managers.Events
{
    public class EventHolder<T>
    {
        public UnityAction<T> OnEvent;

        public EventHolder()
        {

        }
    }
}

