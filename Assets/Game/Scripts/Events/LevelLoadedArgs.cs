using System;

namespace EC.Managers.Events
{
    public class LevelLoadedArgs : EventArgs
    {
        public int tutorialLevelCount;
        public int totalLevelCount;

        public LevelLoadedArgs(int tutorialLevelCount, int totalLevelCount)
        {
            this.tutorialLevelCount = tutorialLevelCount;
            this.totalLevelCount = totalLevelCount;
        }
    }
}

