using EC.Utility;
using UnityEngine;
using UnityEngine.SceneManagement;
using EC.Managers.Events;
using System.Collections;

namespace EC.Managers
{
    public class MainManager : MonoSingleton<MainManager>
    {   
        private MenuManager menuManager;
        [SerializeField] private EditorGameSettings editorGameSettings;
        [SerializeField] private GameManager gameManager;
        [SerializeField] private CurrencyManager currencyManager;
        [SerializeField] private EventManager eventManager;
        [SerializeField] private AssetManager assetManager;
        [SerializeField] private ShopManager shopManager;
        [SerializeField] private SDKManager sdkManager;
        [SerializeField] private SettingsManager settingsManager;
        [SerializeField] private PoolingManager poolingManager;
        private RemoteConfigManager remoteConfigManager = new RemoteConfigManager();

        private string lastLoadedScene = "";
        private GameObject lastLoadedScenePrefab;

        public MenuManager MenuManager { get => menuManager; set => menuManager = value; }
        public GameManager GameManager { get => gameManager; }
        public CurrencyManager CurrencyManager { get => currencyManager; }
        public EventManager EventManager { get => eventManager; }
        public AssetManager AssetManager { get => assetManager; }
        public ShopManager ShopManager { get => shopManager; }
        public SDKManager SdkManager { get => sdkManager; }
        public SettingsManager SettingsManager { get => settingsManager; }
        public string LastLoadedScene { get => lastLoadedScene; set => lastLoadedScene = value; }
        public GameObject LastLoadedScenePrefab { get => lastLoadedScenePrefab; set => lastLoadedScenePrefab = value; }
        public RemoteConfigManager RemoteConfigManager { get => remoteConfigManager; }
        public EditorGameSettings EditorGameSettings { get => editorGameSettings; }
        public PoolingManager PoolingManager { get => poolingManager; }

        private void Awake()
        {
            EditorGameSettings gameSettings = Resources.LoadAll<EditorGameSettings>("Managers")[0];
            Application.targetFrameRate = 60;
            QualitySettings.vSyncCount = 0;
#if UNITY_EDITOR
            if (gameSettings == null || !gameSettings.startFromGameScene)
            {
                Initialize();
            }
#else       
            Initialize();
#endif
        }
        public void Initialize()
        {
            menuManager = FindObjectOfType<MenuManager>();
            eventManager = new EventManager();
            eventManager.Initialize();
            assetManager.Initialize();
            currencyManager.Initialize();
            menuManager.Initialize();
            sdkManager.Initialize();
            settingsManager.Initialize();
            poolingManager.Initialize();
            gameManager.Initialize();
            //EventRunner.LoadSceneStart();
            SceneManager.LoadScene("LevelLoaderScene", LoadSceneMode.Additive);
        }
        public void UnloadScene(string sceneName)
        {
            StartCoroutine(UnloadRoutine(sceneName));
        }

        public IEnumerator UnloadRoutine(string sceneName)
        {
            yield return new WaitForSeconds(1f);
            SceneManager.UnloadScene(sceneName);
        }
    }
}

