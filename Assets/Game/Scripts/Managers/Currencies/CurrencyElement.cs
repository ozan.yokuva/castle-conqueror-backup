using UnityEngine;
using DG.Tweening;
using EC.Managers.Events;
using System.Collections;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace EC.Managers.Currencies
{
    [RequireComponent(typeof(MeshRenderer))]
    public class CurrencyElement : MonoBehaviour
    {
        [SerializeField] private int elementId;
        [SerializeField] private int increasingAmount;
        private bool collected;
        private MeshRenderer meshRenderer;

        public int ElementId { get => elementId; }

        private void Awake()
        {
            this.meshRenderer = GetComponent<MeshRenderer>();
        }
        public void Initialize(int eId)
        {
            this.elementId = eId;
        }

        private void OnTriggerEnter(Collider other)
        {
            Fly();
        }

        [Button("Get Collected")]
        public void Fly()
        {
            if(collected) return;
           
            collected = true;
            Image flyObject = MainManager.Instance.PoolingManager.GetGameObjectById(PoolID.Currency).GetComponent<Image>();
            flyObject.transform.position = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            flyObject.sprite = MainManager.Instance.CurrencyManager.currencies[elementId].currencyImage;

            Transform currencyUI = MainManager.Instance.MenuManager.GamePanel.currencyPanels[elementId].transform;
            flyObject.transform.SetParent(currencyUI, true);
            flyObject.transform.DOLocalMove(Vector3.zero, 1.5f).onComplete = () =>
            {
                flyObject.gameObject.SetActive(false);
            };
            meshRenderer.enabled = false;
            StartCoroutine(PostFly());
        }

        public IEnumerator PostFly()
        {
            yield return new WaitForSeconds(1.4f);
            EventRunner.EarnCurrency(elementId, increasingAmount, true);
            gameObject.SetActive(false);
        }
    }
}

