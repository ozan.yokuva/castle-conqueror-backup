using UnityEngine.UI;
using EC.Managers.Events;

namespace EC.Managers.Menu
{
    public class WinPanel : PanelManager
    {
        public void HandleNextLevel()
        {
            Disappear();
            EventRunner.LoadSceneStart(true);
        }
    }
}

