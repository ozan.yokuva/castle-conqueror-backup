using System.Collections;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

namespace EC.Managers.Menu
{
    [RequireComponent(typeof(CanvasGroup))]
    public class PanelManager : MonoBehaviour
    {
        private CanvasGroup canvasGroup;
        [MinValue(0.01f)]
        public float appearTime;
        [MinValue(0.01f)]
        public float disappearTime;

        private Coroutine appearDisappearCoroutine;

        public virtual void Initialize()
        {
            canvasGroup = GetComponent<CanvasGroup>();
        }
        public virtual void Appear(EventArgs eventArgs = null)
        {
            StopAppearDisappearCoroutine();
            appearDisappearCoroutine = StartCoroutine(AppearRoutine());
        }

        public IEnumerator AppearRoutine()
        {
            while (canvasGroup.alpha < 1)
            {
                canvasGroup.alpha += Time.deltaTime / appearTime;
                yield return new WaitForEndOfFrame();
            }
            canvasGroup.blocksRaycasts = true;
            canvasGroup.interactable = true;
        }

        public virtual void Disappear()
        {
            StopAppearDisappearCoroutine();
            appearDisappearCoroutine = StartCoroutine(DisappearRoutine());
        }

        private void StopAppearDisappearCoroutine()
        {
            if (appearDisappearCoroutine != null)
            {
                StopCoroutine(appearDisappearCoroutine);
            }
        }

        public IEnumerator DisappearRoutine()
        {
            while (canvasGroup.alpha > 0)
            {
                canvasGroup.alpha -= Time.deltaTime / disappearTime;
                yield return new WaitForEndOfFrame();
            }
            canvasGroup.blocksRaycasts = false;
            canvasGroup.interactable = false;
        }
    }
}

