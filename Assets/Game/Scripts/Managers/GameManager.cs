using UnityEngine;
using System;
using EC.Utility;
using UnityEngine.SceneManagement;
using EC.Managers.Events;
using EC.Managers.Scene;
using JsonFx.Json;
using EC.Extensions;

namespace EC.Managers
{
    public class GameManager : MonoBehaviour
    {
        private int tutorialLevelCount;
        private int totalLevelCount;
        public EC.CastleConqueror.GameParameters Parameters;
        int levelId;
        public void Initialize()
        {
       

              Debug.unityLogger.logEnabled = false;

            Parameters.Reset();
            MainManager.Instance.EventManager.Register(EventTypes.LevelRestart, LoadLevel);
            MainManager.Instance.EventManager.Register(EventTypes.LevelFinish, LoadLevel);
            MainManager.Instance.EventManager.Register(EventTypes.LevelSuccess, LevelSuccess);
            MainManager.Instance.EventManager.Register(EventTypes.LevelLoaded, LevelLoaded);
        }
        public int GetPlayerLevel()
        {
            int levelId = PlayerPrefs.GetInt("Level");
            return levelId+1;
        
        }

        //Initialize PlayerController and any other level scene elements
        public void LevelLoaded(EventArgs args)
        {
            
            if(args is LevelLoadedArgs)
            {
                LevelLoadedArgs levelLoadedArgs = args as LevelLoadedArgs;
                tutorialLevelCount = levelLoadedArgs.tutorialLevelCount;
                totalLevelCount = levelLoadedArgs.totalLevelCount;
            }
            ECMonoBehaviour.setLog(true);
            ECMonoBehaviour.Initialize();
        }

        public void LoadLevel(EventArgs args)
        {
           
            SceneManager.LoadScene("LevelLoaderScene", LoadSceneMode.Additive);
        }

        public void LevelSuccess(EventArgs args)
        {
            PlayerPrefs.SetInt("Level", PlayerPrefs.GetInt("Level") + 1);
             levelId = PlayerPrefs.GetInt("Level");
            LevelList levelList = JsonReader.Deserialize<LevelList>(PlayerPrefs.GetString("LevelList"));

            if (levelId == levelList.levels.Count)
            {
                levelList.levels.RemoveRange(0, tutorialLevelCount);
                PlayerPrefs.SetString("LevelList", JsonWriter.Serialize(levelList));
            }
            else if ((levelId - totalLevelCount) % levelList.levels.Count == 0 && levelId > 0)
            {
                levelList.levels.Shuffle();
                PlayerPrefs.SetString("LevelList", JsonWriter.Serialize(levelList));
            }
        }

        public void HandleEarnButton(int earnAmount)
        {
            EventRunner.EarnCurrency(0, earnAmount, true);
        }
    }
}

