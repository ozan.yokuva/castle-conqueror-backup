public enum PoolID
{
	Obstacles,
	Particles,
    Currency,
	MobPlayer,
	MobEnemy,
	DieParticleBlue,
	DieParticleRed,
	PathObject,
	CollectableProjectile,
	MathDoorMultiply,
	MathDoorPlus,
	MathDoorMinus,
	MathDoorDivide,
	RedHead,
	BlueHead,
	

}
