using System;
using EC.Utility;
using EC.Managers.Events;
using UnityEngine;

#if ELEPHANT_SDK_MANAGER_IMPORTED
using ElephantSDK;
#endif

namespace EC.Managers
{
    public class SDKManager : MonoSingleton<SDKManager>
    {
        private bool isInitialized;
        public void Initialize()
        {
            if (!isInitialized)
            {
                MainManager.Instance.EventManager.Register(EventTypes.LevelStart, LogLevelStartEvent);
                MainManager.Instance.EventManager.Register(EventTypes.LevelFail, LogLevelFailEvent);
                MainManager.Instance.EventManager.Register(EventTypes.LevelSuccess, LogLevelCompleteEvent);
                isInitialized = true;
            }
        }

        public void LogLevelCompleteEvent(EventArgs levelNumber)
        {
#if ELEPHANT_SDK_MANAGER_IMPORTED
            Elephant.LevelCompleted(PlayerPrefs.GetInt("Level"));
#endif
        }

        public void LogLevelStartEvent(EventArgs levelNumber)
        {
            int lNumber = (levelNumber as IntArgs).value;
#if ELEPHANT_SDK_MANAGER_IMPORTED
            Elephant.LevelStarted(PlayerPrefs.GetInt("Level") + 1);
#endif
        }
        public void LogLevelFailEvent(EventArgs levelNumber)
        {
#if ELEPHANT_SDK_MANAGER_IMPORTED
            Elephant.LevelFailed(PlayerPrefs.GetInt("Level") + 1);
#endif
        }
    }
}

