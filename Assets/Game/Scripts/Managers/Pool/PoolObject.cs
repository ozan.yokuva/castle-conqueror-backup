using UnityEngine;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif



namespace EC.Managers.Pool
{
    [System.Serializable]
    public class PoolObject
    {
        [OnValueChanged("OnPrefabSet")]
        public GameObject objectPrefab;
        public PoolID poolName;
        public int objectCount;

        public void OnPrefabSet()
        {
#if UNITY_EDITOR
            PoolElement poolElement;
            if(!objectPrefab.TryGetComponent<PoolElement>(out poolElement))
            {
                objectPrefab.AddComponent<PoolElement>();
                EditorUtility.SetDirty(objectPrefab);
                AssetDatabase.SaveAssets();
            }
#endif
        }


    }
}

