using System;
using UnityEngine;

namespace EC.Managers.Scene
{
    public class LevelPrefab : MonoBehaviour, IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}

