using UnityEngine;
using EC.Managers.Events;

namespace EC.Managers
{
    [CreateAssetMenu(fileName = "SettingsManager", menuName = "Scriptlable Objects/Settings Manager")]
    public class SettingsManager : ScriptableObject
    {
        private bool vibOn;

        public bool VibOn { get => vibOn; }

        public void Initialize()
        {
            vibOn = PlayerPrefs.GetInt("VibrationState", 1) == 1;
            MainManager.Instance.EventManager.InvokeEvent(EventTypes.VibrationChange, new BoolArgs(vibOn));
        }

        public void ChangeVib(bool newVib)
        {
            vibOn = newVib;
            PlayerPrefs.SetInt("VibrationState", newVib ? 1 : 0);
            EventRunner.ChangeVibMode(vibOn);
        }
    }
}

