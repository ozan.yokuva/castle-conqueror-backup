using System;
using System.Collections.Generic;
using UnityEngine;
using EC.Managers;

public class DisableOnFinish : ECMonoBehaviour
{
    public override int _PriorityLevel => 10;

    public override void OnSceneLoadComplete()
    {
        gameObject.SetActive(false);
        MainManager.Instance.EventManager.Register(EventTypes.LevelRestart, DisableObject, true);
        MainManager.Instance.EventManager.Register(EventTypes.LevelFinish, DisableObject, true);
    }


    public void DisableObject(EventArgs Args)
    {
        gameObject.SetActive(false);
    }

}
