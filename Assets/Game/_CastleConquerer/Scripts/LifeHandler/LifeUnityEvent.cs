using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EC.Managers;

[RequireComponent(typeof(ILife))]
public class LifeUnityEvent : ECMonoBehaviour
{
    public override int _PriorityLevel => 250;
    public UnityEvent OnLifeChange;
    public UnityEvent OnDeath;
    ILife LifeHandler;
    public void OnLifeChangeTrigger()
    {
        OnLifeChange.Invoke();    
    }
    public void OnDeathTrigger()
    {
        OnDeath.Invoke();
    }


    

    public override void OnSceneLoadComplete()
    {
        LifeHandler = GetComponent<ILife>();
        LifeHandler.OnLifeChanged += OnLifeChangeTrigger;
        LifeHandler.OnDeath += OnDeathTrigger;
    }


    private void OnDestroy()
    {
        if (LifeHandler != null)
        {
            LifeHandler.OnLifeChanged -= OnLifeChangeTrigger;
            LifeHandler.OnDeath -= OnDeathTrigger;
        }
    }
}
