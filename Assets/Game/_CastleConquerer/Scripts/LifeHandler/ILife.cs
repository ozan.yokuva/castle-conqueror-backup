using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface ILife 
{
    Action OnDeath { get; set; }
    Action OnLifeChanged { get; set; }

    void GetDamage(float damage);
    float GetHealt();
    float GetHealtPercent();
    void SetHealt(float healt);
    void Heal(float healt);
    bool IsDead();
    void Die();
}
