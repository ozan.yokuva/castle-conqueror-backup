using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EC.Managers;

namespace EC.CastleConqueror
{
    public class ElephantLoader : ECMonoBehaviour
    {
        GameParameters _Parameters
        {
            get
            {
                return EC.Managers.MainManager.Instance.GameManager.Parameters;
            }
        }
        public override int _PriorityLevel => 0;
        public void loadRemoteParameters()
        {

            if (!_Parameters.EnableElephantRemote)
                return;
           _Parameters.ScreenShakeDuration = MainManager.Instance.RemoteConfigManager.GetFloatConfig("ScreenShakeDuration", _Parameters.ScreenShakeDuration);
           _Parameters.ScreenShakeMagnitude = MainManager.Instance.RemoteConfigManager.GetFloatConfig("ScreenShakeMagnitude", _Parameters.ScreenShakeMagnitude);
           _Parameters.HapticLaunch = MainManager.Instance.RemoteConfigManager.GetIntConfig("HapticLaunch", _Parameters.HapticLaunch);
           _Parameters.HapticReload = MainManager.Instance.RemoteConfigManager.GetIntConfig("HapticReload", _Parameters.HapticReload);
           _Parameters.HapticProjectiteCollusion = MainManager.Instance.RemoteConfigManager.GetIntConfig("HapticProjectiteCollusion", _Parameters.HapticProjectiteCollusion);
           _Parameters.HapticSpawnerDestroyed = MainManager.Instance.RemoteConfigManager.GetIntConfig("HapticSpawnerDestroyed", _Parameters.HapticSpawnerDestroyed);
           _Parameters.MobTargetLockDistance = MainManager.Instance.RemoteConfigManager.GetFloatConfig("MobTargetLockDistance", _Parameters.MobTargetLockDistance);
           _Parameters.ProjectileActivateCollusionCount = MainManager.Instance.RemoteConfigManager.GetIntConfig("ProjectileActivateCollusionCount", _Parameters.ProjectileActivateCollusionCount);
           _Parameters.SwerveIntensity = MainManager.Instance.RemoteConfigManager.GetFloatConfig("SwerveIntensity", _Parameters.SwerveIntensity);
           _Parameters.ReloadDelay = MainManager.Instance.RemoteConfigManager.GetFloatConfig("ReloadDelay", _Parameters.ReloadDelay);
           _Parameters.CollectableExpandTime = MainManager.Instance.RemoteConfigManager.GetFloatConfig("CollectableExpandTime", _Parameters.CollectableExpandTime);
          
           _Parameters.MobSpeed = MainManager.Instance.RemoteConfigManager.GetFloatConfig("MobSpeed", _Parameters.MobSpeed);
            _Parameters.MobScale = MainManager.Instance.RemoteConfigManager.GetFloatConfig("MobScale", _Parameters.MobScale);
            _Parameters.MobBumpScale = MainManager.Instance.RemoteConfigManager.GetFloatConfig("MobBumpScale", _Parameters.MobBumpScale);
            _Parameters.MobHeadDuration = MainManager.Instance.RemoteConfigManager.GetFloatConfig("MobHeadDuration", _Parameters.MobHeadDuration);
            _Parameters.ProjectileActivateMinDuration = MainManager.Instance.RemoteConfigManager.GetFloatConfig("ProjectileActivateMinDuration", _Parameters.ProjectileActivateMinDuration);
            _Parameters.PlusDoorMeltDownDuration = MainManager.Instance.RemoteConfigManager.GetFloatConfig("PlusDoorMeltDownDuration", _Parameters.PlusDoorMeltDownDuration);
            _Parameters.IndicatorDistance = MainManager.Instance.RemoteConfigManager.GetFloatConfig("IndicatorDistance", _Parameters.IndicatorDistance);
            _Parameters.IndicatorCount = MainManager.Instance.RemoteConfigManager.GetIntConfig("IndicatorCount", _Parameters.IndicatorCount);
            _Parameters.IndicatorBumpDelay = MainManager.Instance.RemoteConfigManager.GetFloatConfig("IndicatorBumpDelay", _Parameters.IndicatorBumpDelay);
            _Parameters.IndicatorScale = MainManager.Instance.RemoteConfigManager.GetFloatConfig("IndicatorScale", _Parameters.IndicatorScale);
            _Parameters.IndicatorBumpScale = MainManager.Instance.RemoteConfigManager.GetFloatConfig("IndicatorBumpScale", _Parameters.IndicatorBumpScale);
            _Parameters.ExtraViolence = MainManager.Instance.RemoteConfigManager.GetBoolConfig("ExtraViolence", _Parameters.ExtraViolence);
        }
        public override void OnSceneLoadComplete()
        {
            loadRemoteParameters();
        }
    }
}

