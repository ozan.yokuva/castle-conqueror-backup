using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDistorter : MonoBehaviour
{

    /// <summary>
    ///  There is as garbage Collection issue on this !!!!!
    /// </summary>
    Mesh mesh,OldMesh;
    Vector3[] MeshCache;
    public AnimationCurve MeshDistor;
    public float MinDistance =0 ;
    public float MaxDistance =0 ;
    public float DistanceDifference;
    public float applyHeight;
    public Vector3 applyVector  ;
    public float speed;
    float bumpPoint;

    public float DistortSize = 2;
    public float DistortSmoothing = 0.2f;
    void Start()
    {
        mesh = new Mesh();
        OldMesh = GetComponent<MeshFilter>().mesh;
        GetComponent<MeshFilter>().mesh = mesh;
        mesh.vertices = new Vector3[OldMesh.vertices.Length];
        MeshCache =  new Vector3[mesh.vertices.Length];
        copyMesh();
        MinDistance = OldMesh.bounds.min.y;
        MaxDistance = OldMesh.bounds.max.y;
    }

    public void copyMesh()
    {
        mesh.vertices = OldMesh.vertices;
        mesh.triangles = OldMesh.triangles;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
    }
    // Update is called once per frame
    private void LateUpdate()
    {
        DistanceDifference = (MaxDistance - MinDistance);
        applyHeight =MinDistance + ((applyHeight + Time.deltaTime* speed) % DistanceDifference) ;
        doDistort();
    }

    void distort(ref Vector3 TargetVertice ,ref Vector3 StartVertice, ref Vector3 normal)
    {
         bumpPoint = (MaxDistance + applyHeight - StartVertice.y) % MaxDistance; 
        applyVector  = StartVertice + normal * MeshDistor.Evaluate(bumpPoint / (MaxDistance-MinDistance))*DistortSize;
        TargetVertice = Vector3.Lerp(StartVertice, applyVector, DistortSmoothing);
    }
    public void doDistort()
    {
        
        for (int i = 0; i < MeshCache.Length; i++)
            distort(ref MeshCache[i] , ref OldMesh.vertices[i], ref OldMesh.normals[i]);
        mesh.SetVertices(MeshCache);
    
    }

}
