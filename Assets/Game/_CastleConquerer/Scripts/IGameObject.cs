using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameObject 
{
    GameObject GetGameObject();
    void Inheritance(IGameObject Mother);

    void Reset();
}
