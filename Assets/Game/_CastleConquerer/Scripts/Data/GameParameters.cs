using UnityEngine;
using Sirenix.OdinInspector;
namespace EC.CastleConqueror
{
    [CreateAssetMenu(fileName = "GameParameters" , menuName = "Scriptables/Data/GameParameters")]
    public class GameParameters : ScriptableObject
    {

        public bool LogEnabled=false;
        //public float PlayerMoveSpeed;
        public Vector3 CameraOffset;
        public Vector3 CameraOffsetAdd;
        public Vector3 CameraAngle;
        public float CameraSmoothing = 1;
        public GameState State;
        public float ScreenShakeDuration = 0.1f;
        public float ScreenShakeMagnitude = 0.1f;
        public int HapticLaunch = 1;
        public int HapticReload = 1;
        public int HapticProjectiteCollusion = 1;
        public int HapticSpawnerDestroyed = 1;
        public bool EnableElephantRemote = true;
        public float MobTargetLockDistance = 5;
        public Vector3 ProjectileForceMin;
        public Vector3 ProjectileForceMax;
        public int ProjectileActivateCollusionCount = 2;
        public float ProjectileActivateMinDuration = .5f;
        public float PlusDoorMeltDownDuration = .5f;

        public float SwerveIntensity;
        public float ReloadDelay=1f;
        public float CollectableExpandTime = .5f;
        public float MobSpeed = 1;
        public float MobScale = 1;
        public float MobBumpScale = 2;
        public float MobHeadDuration = 2;
        public float IndicatorDistance = 8f ;
        public int IndicatorCount = 20;
        public float IndicatorBumpDelay = .2f;
        public float IndicatorScale = .5f;
        public float IndicatorBumpScale = .5f;
        public bool ExtraViolence = false;
        public void Reset()
        {
            State = GameState.First;
            CameraOffsetAdd = Vector3.zero;
        }
    }


    public enum GameState
    {
        First,
        Play,
        Failed,
        Win
    }
}
