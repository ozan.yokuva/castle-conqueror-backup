﻿using System;
using UnityEngine;
using EC.Managers;
namespace EC.CastleConqueror

{

    public class Billboard : ECMonoBehaviour
    {
        public Camera Cam;
        public Camera _Cam
        {
            get
            {
                if (Cam == null)
                    Cam = Camera.main;

                return Cam;
            }
        }

        public override int _PriorityLevel => 100;

        private void LateUpdate()
        {

            if (_Cam != null)
                transform.LookAt(transform.position + Cam.transform.forward);
        }

       
        public override void OnSceneLoadComplete()
        {  
            if (Cam == null)
                Cam = Camera.main;
           
        }
        // Update is called once per frame

    }
}