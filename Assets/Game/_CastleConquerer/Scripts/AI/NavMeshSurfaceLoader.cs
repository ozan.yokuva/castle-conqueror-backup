using UnityEngine.AI;
using EC.Managers;

namespace EC.CastleConqueror
{
    public class NavMeshSurfaceLoader : ECMonoBehaviour
    {
        NavMeshSurface _NavMeshSurface;
        public override int _PriorityLevel => 80;
        public override void OnSceneLoadComplete()
        {
            _NavMeshSurface = GetComponent<NavMeshSurface>();
            _NavMeshSurface.buildHeightMesh = true;
            _NavMeshSurface.BuildNavMesh();
        }
    }
}
