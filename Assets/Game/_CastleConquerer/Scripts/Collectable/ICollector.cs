namespace EC.CastleConqueror
{
    public interface ICollector : IGameObject
    {
        bool Collect(ICollectable Bonus);
    }
}
