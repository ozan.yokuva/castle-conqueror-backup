using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EC.Managers;
using EC.Utility;
using DG.Tweening;
using TMPro;


namespace EC.CastleConqueror
{
    public class Collectable : ECMonoBehaviour, ICollectable
    {
        [SerializeField] CollusionType _CollusionType;
        [SerializeField] float _Duration;
        [SerializeField] int _Uses;
        [SerializeField] string EffectSign;
        [SerializeField] Material ProjectiteMaterial;
        [SerializeField] TextMeshPro Text;
        int _UsesBackup;
        GameParameters _Paramaters
        {
            get
            {
                return MainManager.Instance.GameManager.Parameters;

            }
        }
        List<IEffect> _Effects;
        Vector3 OriginalScale;

        float _ActiveValue;
        int CollectableId = 2;
        bool _IsShotDown = false;
        public override int _PriorityLevel => 95;
        public GameObject GetGameObject()
        {
            return gameObject;
        }
        public int GetID()
        {
            return CollectableId;
        }
        public override void OnSceneLoadComplete()
        {
            if (OriginalScale == Vector3.zero)
                OriginalScale = transform.localScale;
            Reset();
        }
        public void Reset()
        {
            transform.position = Vector3.one * 99999;
            Text.text = EffectSign + _ActiveValue;
            _IsShotDown = false;
            transform.localScale = OriginalScale;
            if (_Effects == null)
            {
                _Effects = new List<IEffect>();
                _Effects.AddRange(GetComponentsInChildren<IEffect>());
            }
        }
        public void SetID(int ID)
        {
            CollectableId = ID;
        }
        public void StartAction()
        {
            Reset();
            Delayer.Delay(_Duration, ShutDown);
            transform.localScale = Vector3.zero;
            GetComponent<Collider>().enabled = false;
            Tween tw = transform.DOScale(OriginalScale, _Paramaters.CollectableExpandTime);
            tw.onComplete = () => { GetComponent<Collider>().enabled = true; };

        }
        public void ShutDown()
        {
            if (_IsShotDown)
                return;

            transform.DOScale(Vector3.zero, _Paramaters.CollectableExpandTime);
            _IsShotDown = true;
            Delayer.Delay(_Paramaters.CollectableExpandTime + 0.1f, () =>
             {
                 gameObject.SetActive(false);
                 _Uses = _UsesBackup;
             }
            );
        }

        private void OnTriggerEnter(Collider other)
        {
            doTouch(other);

        }
        private void OnTriggerStay(Collider other)
        {
            
        
            doTouch(other);

        }

        public void doTouch(Collider other)
        { 
         if (other.gameObject.layer == 6)
            {
                if (_CollusionType == CollusionType.TriggerEnter && _Uses > 0)
                {
                    if (other.attachedRigidbody == null)
                        return;

                    ICollector Collector = other.attachedRigidbody.GetComponent<ICollector>();
                    if (Collector != null)
                    {
                        if (_Effects == null)
                        {
                            _Effects = new List<IEffect>();
                            _Effects.AddRange(GetComponentsInChildren<IEffect>());
                        }
                        foreach (IEffect effect in _Effects)
                        {
                            effect.Effect(this, Collector);
    }
}
                }
            }


        }


        public void Use(int amount)
        {
            _Uses-=amount;
            if (_Uses <= 0)
                ShutDown();
            else if (_Uses < 200)
                Text.text = EffectSign + _Uses;
        }

        public void SetActiveValue(float activeValue)
        {
            _UsesBackup = _Uses;
            _ActiveValue = activeValue;
            if (_Effects == null)
            {
                _Effects = new List<IEffect>();
                _Effects.AddRange(GetComponentsInChildren<IEffect>());
            }

            foreach (IEffect effect in _Effects)
            {
                effect.SetActiveValue(this,activeValue);
            }
        }

        public void SetUse(int amount)
        {
            _Uses = amount;
        }

        public Material GetMaterial()
        {
            return ProjectiteMaterial;
        }

        public string GetText()
        {
            return EffectSign + Mathf.CeilToInt(_ActiveValue);
        }
        
        public void Inheritance(IGameObject Mother){}

        public int  GetUse()
        {
            return _Uses;
        }
    }
    public enum CollusionType
    {
      //  CollusionEnter,
       // CollusionStay,
        TriggerEnter,
       // TriggerStay
    }
}
