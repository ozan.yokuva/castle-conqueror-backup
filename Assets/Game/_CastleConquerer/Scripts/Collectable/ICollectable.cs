using UnityEngine;

namespace EC.CastleConqueror
{
    public interface ICollectable : IGameObject
    {
        int GetID();
        
        void SetID(int ID);
        void StartAction();
        void Use(int amount);
        void SetUse(int amount);
        int GetUse();
        void SetActiveValue(float activeValue);

        public Material GetMaterial();
        public string GetText();
    }
}
