using System.Collections.Generic;
using System.Collections;
using EC.Managers;
using EC.Managers.Pool;
using UnityEngine;

namespace EC.CastleConqueror
{
    public class EffectAdd : MonoBehaviour, IEffect
    {
        [SerializeField] PoolID _SpawnParticle;
        [SerializeField] int _SpawnAmount;
        [SerializeField] float _SpawnRange;
        [SerializeField] int _Use;
        ICollectable _Effector;
        GameObject _SpawnGameObject;
        Vector3 _TempVector3;
        PoolID _SpawnPoolID;
        float _MeltDownDuration => MainManager.Instance.GameManager.Parameters.PlusDoorMeltDownDuration;

        public void Effect(ICollectable Effector, ICollector Effected)
        {

            _Effector = Effector;
            _SpawnPoolID = Effected.GetGameObject().GetComponent<PoolElement>().PoolId;
            
            _Effector.GetGameObject().GetComponentInChildren<Collider>().enabled = false;
            if (Effected.Collect(Effector))
                StartCoroutine(CreateMobs(Effector, Effected));
        }

        public void SetActiveValue(ICollectable Collectable, float ActiveValue)
        {
            _SpawnAmount = Mathf.CeilToInt(ActiveValue);
             Collectable.SetUse(_SpawnAmount);
        }
        
        IEnumerator CreateMobs(ICollectable Effector, ICollector Effected)
        {

            for (int i = 0; i < _SpawnAmount; i++)
            {
                _TempVector3 = Random.insideUnitSphere * _SpawnRange;
                _TempVector3.y = 0;
                _SpawnGameObject = MainManager.Instance.PoolingManager.GetGameObjectById(_SpawnPoolID);
                if (_SpawnGameObject.GetComponent<ICollector>().Collect(Effector))
                {

                    _SpawnGameObject.GetComponent<ICollector>().Inheritance(Effected);
                    _SpawnGameObject.GetComponent<ISpawnable>().SetPosition(Effected.GetGameObject().transform.position +
                        _TempVector3);
                    _SpawnGameObject.GetComponent<ISpawnable>().StartAction();
                    Effector.Use(1);
                }
                else
                    _SpawnGameObject.SetActive(false);
                yield return new WaitForSeconds(_MeltDownDuration / _SpawnAmount );
            }



        }
    }
}
