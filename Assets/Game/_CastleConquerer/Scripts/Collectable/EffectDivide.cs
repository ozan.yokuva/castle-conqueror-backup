using EC.Managers;
using UnityEngine;

namespace EC.CastleConqueror
{
    //[SerializeField] bool 
    public class EffectDivide : ECMonoBehaviour, IEffect
    {
        int count = 0;
        public override int _PriorityLevel => 100;
        public void Effect(ICollectable Effector, ICollector Effected)
        {
            if(Effected.Collect(Effector))
            {
                if (count % Effector.GetUse() != 0)
                    Effected.GetGameObject().GetComponent<ILife>().Die();
            }

            count++;
        }
        public void SetActiveValue(ICollectable Collectable, float ActiveValue)
        {
            Collectable.SetUse(Mathf.CeilToInt(ActiveValue));
        }
        public override void OnSceneLoadComplete() { }

    }
}
