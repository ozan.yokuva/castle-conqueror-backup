using EC.Managers;
using UnityEngine;

namespace EC.CastleConqueror
{
    //[SerializeField] bool 
    public class EffectMinus: ECMonoBehaviour, IEffect
    {
        public override int _PriorityLevel => 100;
        public void Effect(ICollectable Effector, ICollector Effected)
        {
            Effected.GetGameObject().GetComponent<ILife>().Die();
            Effector.Use(1);
        }
        public void SetActiveValue(ICollectable Collectable, float ActiveValue)
        {
            Collectable.SetUse(Mathf.CeilToInt(ActiveValue));
        }
        public override void OnSceneLoadComplete() { }

    }
}
