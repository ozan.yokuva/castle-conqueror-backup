using System.Collections.Generic;
using System.Collections;
using EC.Managers;
using EC.Managers.Pool;
using UnityEngine;

namespace EC.CastleConqueror
{
    public class EffectMultiply : MonoBehaviour, IEffect
    {
        [SerializeField] PoolID _SpawnParticle;
        [SerializeField] int _SpawnAmount;
        [SerializeField] float _SpawnRange;
        [SerializeField] int _Use;
        ICollectable _Effector;
        GameObject _SpawnGameObject;
        Vector3 _TempVector3;
        PoolID _SpawnPoolID;

        public void Effect(ICollectable Effector, ICollector Effected)
        {
            _Effector = Effector;
            //Effector.Use(_SpawnAmount);
            _SpawnPoolID = Effected.GetGameObject().GetComponent<PoolElement>().PoolId;
            if (Effected.Collect(Effector))
                for (int i = 0; i < _SpawnAmount - 1; i++)
                {
                    _TempVector3 = Random.insideUnitSphere * _SpawnRange;
                    _TempVector3.y = 0;
                    _SpawnGameObject = MainManager.Instance.PoolingManager.GetGameObjectById(_SpawnPoolID);
                    if (_SpawnGameObject.GetComponent<ICollector>().Collect(Effector))
                    {

                        _SpawnGameObject.GetComponent<ICollector>().Inheritance(Effected);
                        _SpawnGameObject.GetComponent<ISpawnable>().SetPosition(Effected.GetGameObject().transform.position +
                            _TempVector3);
                        _SpawnGameObject.GetComponent<ISpawnable>().StartAction();

                    }
                    else
                        _SpawnGameObject.SetActive(false);
                }
        }

        public void SetActiveValue(ICollectable Collectable, float ActiveValue)
        {
            _SpawnAmount = Mathf.CeilToInt(ActiveValue);
            // Collectable.SetUse(_Use);
        }

    }
}
