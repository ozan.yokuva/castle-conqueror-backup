namespace EC.CastleConqueror
{
    public interface IEffect
    {

        public void Effect(ICollectable Effector, ICollector Effected);
        public void SetActiveValue(ICollectable Collectable, float ActiveValue);

    }
}
