
using UnityEngine;
using EC.Managers;
using System.Collections.Generic;
using System;
using System.Collections;

namespace EC.CastleConqueror
{
    public class PlayerBase : ECMonoBehaviour
    {

        GameParameters _Parameters
        {
            get
            {
                return MainManager.Instance.GameManager.Parameters;

            }
        }
        [SerializeField] string EnemyTargetsTag;
        [SerializeField] string PlayerTargetsTag;

        List<ITarget> EnemyTargets;
        List<ITarget> PlayerTargets;
        GameObject[] TargetTemp;
        public IPlayerInputAdapter InputAdapter;
        public IPlayerInputAdapter _InputAdapter {
            get { 
                if(InputAdapter==null)
                    InputAdapter = GetComponent<IPlayerInputAdapter>(); 
                return InputAdapter;
            } 
        } // References Need To Be solved

        bool _Spring  = false;
      

        public override int _PriorityLevel => 1;
        PlayerProjectileManager ProjectileManager;
       
        public Vector3 ShootForce;

        void LoadTargets()
        {
            EnemyTargets = new List<ITarget>();
            PlayerTargets = new List<ITarget>();

            TargetTemp = GameObject.FindGameObjectsWithTag(EnemyTargetsTag);
            for (int i = 0; i < TargetTemp.Length; i++)
            {
                EnemyTargets.Add(TargetTemp[i].GetComponent<ITarget>());
            }


            TargetTemp = GameObject.FindGameObjectsWithTag(PlayerTargetsTag);
            for (int i = 0; i < TargetTemp.Length; i++)
            {
                PlayerTargets.Add(TargetTemp[i].GetComponent<ITarget>());
            }


        }
        public override void OnSceneLoadComplete()
        {

         
            _Parameters.Reset();
            ProjectileManager = GetComponent<PlayerProjectileManager>();
            InputAdapter = GetComponent<IPlayerInputAdapter>();
            LoadTargets();
            MainManager.Instance.EventManager.Register(EventTypes.LevelStart,startPlaying,true);
        }

        void startPlaying(EventArgs Args)
        {
            _Parameters.State = GameState.Play;
        }
        
        bool checkTargets(List<ITarget> targetList)
        {
            if (targetList == null)
            {
                return true; // Could Not Load Yet...
            }
            for (int i = 0; i < targetList.Count; i++)
                if (targetList[i].IsActive())
                    return true;
          

            return false;
        }

        void checkIfFinished()
        {
            if (!checkTargets(EnemyTargets))
            {
                _Parameters.State = GameState.Win;
                MainManager.Instance.EventManager.InvokeEvent(EventTypes.LevelSuccess);
                StartCoroutine(killAll("EnemyMob", .02f));

            }
            else if (!checkTargets(PlayerTargets))
            {
                _Parameters.State = GameState.Failed;
                MainManager.Instance.EventManager.InvokeEvent(EventTypes.LevelFail);
                Shake.Instance.ShakeIt(_Parameters.ScreenShakeDuration, _Parameters.ScreenShakeMagnitude);
                StartCoroutine(killAll("PlayerMob", .02f));



            }
        }

        public void calculateForce()
        {
            if (_InputAdapter.isHold())
            {
                ShootForce.x = Mathf.Clamp(ShootForce.x + _InputAdapter.getInput().x *
                           _Parameters.SwerveIntensity,
                           _Parameters.ProjectileForceMin.x, _Parameters.ProjectileForceMax.x);
                ShootForce.y = Mathf.Clamp(ShootForce.y + _InputAdapter.getInput().y *
                    _Parameters.SwerveIntensity,
                    _Parameters.ProjectileForceMin.y, _Parameters.ProjectileForceMax.y);
                ShootForce.z = Mathf.Clamp(ShootForce.z + _InputAdapter.getInput().y *
                    _Parameters.SwerveIntensity,
                    _Parameters.ProjectileForceMin.z, _Parameters.ProjectileForceMax.z);
            }

        }

        public void resetForce()
        { 
            ShootForce.x = 0;
            ShootForce.y = _Parameters.ProjectileForceMin.y;
            ShootForce.z = _Parameters.ProjectileForceMin.z;
        }
       void Update()
        {
            if (_Parameters.State == GameState.Play)
            {
                calculateForce();
                checkIfFinished();
                if (_Spring &&Input.GetMouseButtonUp(0))
                {
                    _Spring = false;
                    ProjectileManager.Fire(ShootForce,+_Parameters.ReloadDelay);

                    resetForce();
                }
                if ( Input.GetMouseButtonDown(0))
                {
                    _Spring = true;
                }
            }
        }
        IEnumerator killAll(string killTag , float sleep) {


            GameObject[] Allies = GameObject.FindGameObjectsWithTag(killTag);
            for (int i = 0; i < Allies.Length; i++)
            {
                if (Allies[i].GetComponent<MobBase>() != null)
                {
                    Allies[i].GetComponent<MobBase>().Die();
                    yield return new WaitForSeconds(sleep);
                }
            }

            
        }
    }
}
