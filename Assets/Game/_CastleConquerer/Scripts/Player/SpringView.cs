using UnityEngine;
using EC.Managers;


namespace EC.CastleConqueror
{
    public class SpringView : ECMonoBehaviour
    {
        public override int _PriorityLevel => 100;
        [SerializeField]SkinnedMeshRenderer _SpringMesh;
        [SerializeField]PlayerBase _Player;
        [SerializeField] AnimationCurve SpringWawe;

        float _SpringEnd;
        GameParameters _Parameters {
            get {
                return MainManager.Instance.GameManager.Parameters;  
            }
        }
        bool _HoldCache = false;
        bool _IsHold {
            get
            {
                if (_Parameters.State == GameState.Play)
                {
                    if (_Player.InputAdapter != null)
                    {
                        if (_HoldCache != _Player.InputAdapter.isHold())
                        {
                            if (_HoldCache)
                            {
                                startSpring();
                            }
                            _HoldCache = _Player.InputAdapter.isHold();
                        }
                    }
                    else
                        Debug.Log("InputAdapterIsNull");
                }
                return _HoldCache;
            }
        }

        public void startSpring() {
            
            _SpringEnd = Time.time + _Parameters.ReloadDelay;
        }
        void Update()
        {
            float degree = Mathf.Rad2Deg *  Mathf.Atan2(_Player.ShootForce.x, _Player.ShootForce.z);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, degree, 0), 0.05f);

            if (_IsHold)
            {
                _SpringMesh.SetBlendShapeWeight(0, 60f + (40 * _Player.ShootForce.y / _Parameters.ProjectileForceMax.y));
                _SpringMesh.SetBlendShapeWeight(1, 100f * (_Parameters.ProjectileForceMax.y - _Player.ShootForce.y) / _Parameters.ProjectileForceMax.y);
            }
            else if (_SpringEnd > Time.time)
            {

                float progress = (_SpringEnd - Time.time) / _Parameters.ReloadDelay;
                _SpringMesh.SetBlendShapeWeight(0, SpringWawe.Evaluate(progress)*100 );
                _SpringMesh.SetBlendShapeWeight(1, SpringWawe.Evaluate(1-progress) * 100);
            }
            else
            {
                _SpringMesh.SetBlendShapeWeight(0, 60f + (40 * _Player.ShootForce.y / _Parameters.ProjectileForceMax.y));
                _SpringMesh.SetBlendShapeWeight(1, 100f * (_Parameters.ProjectileForceMax.y - _Player.ShootForce.y) / _Parameters.ProjectileForceMax.y);
            }
        }
        public override void OnSceneLoadComplete(){}
    }
}
