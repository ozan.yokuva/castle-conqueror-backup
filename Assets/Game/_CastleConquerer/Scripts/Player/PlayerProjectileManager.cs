
using UnityEngine;
using EC.Utility;
using EC.Managers;

namespace EC.CastleConqueror
{
    public class PlayerProjectileManager : ECMonoBehaviour
    {
        public override int _PriorityLevel => 90;
        [SerializeField] Transform CurrentPosRef, NextPosRef,_StartPositionTop,_StartPosition;
        [SerializeField] PlayerBase _Player;
        [SerializeField] AnimationCurve TransferCurve;
        GameParameters _Parameters
        {
            get
            {
                return MainManager.Instance.GameManager.Parameters;
            }
        }

        IProjectile Current, Next;
        CollectableSource _CollectableSource;
        GameObject TempGameObject;
        Rigidbody TempRigidBody;
        int ProjectileId;
        float _NextLoadedTime;
        float _LastDelay;

        public float getTimeLeftToNext()
        {
            return Mathf.Clamp(_NextLoadedTime - Time.time, 0f, 5f);
        }
        public float getTimeLeftpercent()
        {
            return Mathf.Clamp((_NextLoadedTime - Time.time) / _LastDelay, 0f, 1f);
        }
        void loadNew()
        {
            TempGameObject =MainManager.Instance.PoolingManager.GetGameObjectById(PoolID.CollectableProjectile);
            TempRigidBody = TempGameObject.GetComponent<Rigidbody>();
            TempRigidBody.velocity = Vector3.zero;
            TempRigidBody.angularVelocity = Vector3.zero;
            TempRigidBody.transform.position = NextPosRef.transform.position;
            TempRigidBody.transform.rotation = Quaternion.identity;

            //TempRigidBody.isKinematic = true;
            //TempRigidBody.useGravity = false;

            Next = TempGameObject.GetComponent<IProjectile>();
            Next.SetID(ProjectileId); // That ID solves Multiple Uses Do NOT LOOSE IT !!!
            ProjectileId++;
        }

        void swapNext()
        {
            if (Next == null)
                loadNew();
            Current = Next;
            HapticController.Instance.Vibrate((MoreMountains.NiceVibrations.HapticTypes)_Parameters.HapticReload);

            Current.GetGameObject().transform.position = CurrentPosRef.position;
            loadNew();
            Next.GetGameObject().transform.position = NextPosRef.position;
        }

        void checkLoad()
        {
            if (Current == null)
            {
                swapNext();
            }
        }
        public void Fire(Vector3 force , float delayNext)
        {
            if (Current != null)
            {
                HapticController.Instance.Vibrate((MoreMountains.NiceVibrations.HapticTypes)_Parameters.HapticLaunch);
                TempRigidBody = Current.GetGameObject().GetComponent<Rigidbody>();
                TempRigidBody.isKinematic = false;
                TempRigidBody.useGravity = true;
                TempRigidBody.AddForce(force, ForceMode.Impulse);
                Delayer.Delay(.5f, Current.Fire);
                Current = null;
                Delayer.Delay(delayNext, checkLoad);
                _NextLoadedTime = Time.time + delayNext;
                _LastDelay = delayNext;
            }
            
                
        }
        public override void OnSceneLoadComplete()
        {
            _CollectableSource = FindObjectOfType<CollectableSource>();
            checkLoad();
        }
        private void LateUpdate()
        {
            if (_Parameters.State == GameState.Play)
            {
                if (_NextLoadedTime > Time.time)
                {
                    float progress = (_LastDelay - (_NextLoadedTime - Time.time)) / _LastDelay;
                    Next.GetGameObject().transform.position = Vector3.Lerp(NextPosRef.position, CurrentPosRef.position, progress) + TransferCurve.Evaluate(progress) * Vector3.up;
                }
                else if (Current != null && _Player.InputAdapter != null && _Player.InputAdapter.isHold())
                {
                    float positionProgress = (_Parameters.ProjectileForceMax.y - _Player.ShootForce.y) / _Parameters.ProjectileForceMax.y;
                    Current.GetGameObject().transform.position = Vector3.Lerp(_StartPosition.position, _StartPositionTop.position, positionProgress);
                    CurrentPosRef.position = Current.GetGameObject().transform.position;
                }
                else
                {

                    CurrentPosRef.position = Vector3.Lerp(_StartPosition.position, _StartPositionTop.position, .6f);
                    if (Current != null)
                        Current.GetGameObject().transform.position = CurrentPosRef.position;
                    else
                        swapNext();
                }
            }

        }
        
    }
}
