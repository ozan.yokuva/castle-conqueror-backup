using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EC.Managers;
using EC.Utility;

namespace EC.CastleConqueror
{
    public class PathDrawer : ECMonoBehaviour
    {
        public override int _PriorityLevel => 220;
        public AnimationCurve PathObjectSizeCurve;
        GameParameters _Parameters
        {
            get {
                return MainManager.Instance.GameManager.Parameters;
            }
        }
        [SerializeField] PoolID PathDrawObject;
        [SerializeField] PlayerBase _Player;
        [SerializeField] Transform EndPointTransform, startPointTransform;
            
        List<GameObject> PathObjects = new List<GameObject>();
        Vector3 TargetScale, NoWhere = new Vector3(9999, 9999, 9999);
        Vector3 HeightCalculateVector; 
        GameObject TempPathObject;
        int PathObjectRequired;
        int bumpIndex;
        
       // public float traveltime;
        //bool drawing = false;
        //bool draw;
        
        public float getHeight(float vel , float time)
        {
            return  (float) (vel * time + (0.5 * Physics.gravity.y * time * time));
        }
        public void drawTrajectory()
        {
            for (int i = 0; i < _Parameters.IndicatorCount; i++)
            { 
                HeightCalculateVector.y  = getHeight(_Player.ShootForce.y, _Parameters.IndicatorDistance * Time.fixedDeltaTime * i );
                HeightCalculateVector.x = _Player.ShootForce.x * _Parameters.IndicatorDistance * Time.fixedDeltaTime * i;
                HeightCalculateVector.z = _Player.ShootForce.z * _Parameters.IndicatorDistance * Time.fixedDeltaTime * i;
                PathObjects[i].transform.position = startPointTransform.position + HeightCalculateVector;
            }
            AnimateTrajectory();
        }
        public void AnimateTrajectory()
        {
         
            for (int i = 0; i < _Parameters.IndicatorCount; i++)
            {
                
                int bumpPoint = (_Parameters.IndicatorCount + bumpIndex - i) % _Parameters.IndicatorCount;
                TargetScale = Vector3.one* _Parameters.IndicatorScale  + Vector3.one * PathObjectSizeCurve.Evaluate((float)bumpPoint / _Parameters.IndicatorCount) * _Parameters.IndicatorBumpScale;
                PathObjects[i].transform.localScale = Vector3.Lerp(PathObjects[i].transform.localScale, TargetScale, 0.2f);
            }
       
        }
        public void PathObjectsListCheck()
        {

            PathObjectRequired = _Parameters.IndicatorCount;
            while (PathObjects.Count < PathObjectRequired)
            {
                TempPathObject = MainManager.Instance.PoolingManager.GetGameObjectById(PathDrawObject);
                TempPathObject.transform.position = NoWhere;
                PathObjects.Add(TempPathObject);
            }
            while (PathObjects.Count > PathObjectRequired)
            {
                TempPathObject = PathObjects[PathObjects.Count - 1];
                PathObjects.Remove(TempPathObject);
                TempPathObject.SetActive(false);
            }
        }

        void LateUpdate()
        {
            if (_Parameters.State == GameState.Play)
                if (_Player._InputAdapter.isHold())
                {
                    PathObjectsListCheck();
                    drawTrajectory();
                }
                else
                {
                    clear();
                }
        }
        void clear()
        {
            for (int i = 0; i < PathObjects.Count; i++)
            { 
                if (PathObjects[i].transform.position == NoWhere)
                    return;
                PathObjects[i].transform.position = NoWhere;
            }
        }
        void iterateDrawBump()
        {
            bumpIndex = (bumpIndex + 1) % _Parameters.IndicatorCount;
            Delayer.Delay(_Parameters.IndicatorBumpDelay, iterateDrawBump);
        }
        public override void OnSceneLoadComplete()
        {
            iterateDrawBump();
         
        }
    }
}
