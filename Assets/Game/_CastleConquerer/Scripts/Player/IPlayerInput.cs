using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EC.CastleConqueror
{
    public interface IPlayerInputAdapter
    {
        public Vector3 getInput();
        public bool isHold();

    }
    
}
