using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EC.CastleConqueror
{
    public class InputBasicSwerve : MonoBehaviour, IPlayerInputAdapter
    {
       Vector2 screenStart, screenDelta;
        bool _IsHold;

        public Vector3 getInput()
        {
            return (Vector3)screenDelta/Screen.width;
        }

        public bool isHold()
        {
            return _IsHold;
        }

        private void ListeningTouch()
        {
            screenDelta = Vector2.zero;
            


            if (Input.GetMouseButtonDown(0))
            {
                screenStart = Input.mousePosition;
                _IsHold = true;
            }

            if (Input.GetMouseButton(0))
            {
                screenDelta = (Vector2)Input.mousePosition - screenStart;
                screenStart = Input.mousePosition;
                _IsHold = true;

            }
            if (Input.GetMouseButtonUp(0))
            {
                _IsHold = false;
            }

        }

        void Update()
        {
            ListeningTouch();
        }



    }
}
