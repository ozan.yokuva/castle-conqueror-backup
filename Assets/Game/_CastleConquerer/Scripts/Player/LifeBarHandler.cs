using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EC.Managers;
using System;

namespace EC.CastleConqueror
{
    public class LifeBarHandler :ECMonoBehaviour ,ILife
    {
        public override int _PriorityLevel => 100;
        public event Action _OnDeathEvent;
        public event Action _OnLifeChangedEvent;
        public Action OnDeath { get => _OnDeathEvent; set => _OnDeathEvent = value; }
        public Action OnLifeChanged { get => _OnLifeChangedEvent; set => _OnLifeChangedEvent = value; }

        [SerializeField] string _AllyTargetsTag;
        List<GameObject> _AllyTargetsGameObjects;
        List<ILife> _AllyTargetsLife;
        float _TotalMax;

        public float GetHealtPercent()
        {
            _TotalMax = 0;
            for (int i = 0; i < _AllyTargetsGameObjects.Count; i++)
                 _TotalMax += _AllyTargetsLife[i].GetHealtPercent() / _AllyTargetsGameObjects.Count;
            
            return _TotalMax;
        }
        public override void OnSceneLoadComplete()
        {
            _AllyTargetsGameObjects = new List<GameObject>();
            _AllyTargetsGameObjects.AddRange(GameObject.FindGameObjectsWithTag(_AllyTargetsTag));
            _AllyTargetsLife = new List<ILife>();
            for (int i = 0; i < _AllyTargetsGameObjects.Count; i++)
            {
                _AllyTargetsLife.Add(_AllyTargetsGameObjects[i].GetComponent<ILife>());
                _AllyTargetsLife[i].OnLifeChanged += updateHealt;
            }
        }

        public void Die(){}
        public void GetDamage(float damage){}
        public float GetHealt() { return 1; }
        public void Heal(float healt) { }
        public bool IsDead(){return false;}
        public void SetHealt(float healt){}
        void updateHealt()
        {
            OnLifeChanged.Invoke();
        }

    }
}
