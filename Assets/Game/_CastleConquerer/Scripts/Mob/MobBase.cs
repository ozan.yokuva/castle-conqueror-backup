
using UnityEngine;
using UnityEngine.AI;
using EC.Managers;
using EC.Utility;
using System.Collections.Generic;
using System;
using DG.Tweening;
using FSG.MeshAnimator.Snapshot;

namespace EC.CastleConqueror
{
    public class MobBase : ECMonoBehaviour, ISpawnable,ILife,ICollector
    {
        SnapshotMeshAnimator _MeshAnimator;
        public override int _PriorityLevel => 300;
        public GameObject[] TargetObjects;
        public GameObject CurrentTargetObject;
        public bool isDead = false;
        public Action OnDeath { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public Action OnLifeChanged { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        
        [SerializeField] string TargetsTag = "EnemyTarget";
        [SerializeField] float lifeMax;
        [SerializeField] public PoolID DeathParticle;
        [SerializeField] public PoolID Head;


        float life;
        TargetComparer targetComparer;
        ParticleSystem deathParticle;
        GameObject headObject;
        ITarget[] Targets;
        ITarget _CurrentTarget;
        MobAI _AI;
        MobAI AI { 
                get { 
                        if(_AI == null )
                            _AI= GetComponent<MobAI>(); 
                        return _AI; 
                }
        }
        List<int> BonusesPicked;
        Vector3 _TempVector;
        float MobScale => MainManager.Instance.GameManager.Parameters.MobScale;
        float MobBumpScale => MainManager.Instance.GameManager.Parameters.MobBumpScale;
        public GameObject GetGameObject()
        {
            return gameObject;
        }
        public void StartAction()
        {
            GetTargets();
            Action();
            _TempVector = transform.localScale;
            transform.localScale *= MobBumpScale/ MobScale;
            GetComponent<NavMeshAgent>().radius *= MobScale / MobBumpScale; ;
            transform.DOScale(_TempVector, .5f);
          
            Delayer.Delay(0.5f, () =>
            {
                GetComponent<NavMeshAgent>().radius *= 2f;
                
            });

        }
        void GetTargets()
        {
            TargetObjects = GameObject.FindGameObjectsWithTag(TargetsTag);
            Targets = new ITarget[TargetObjects.Length];
            for (int i = 0; i < TargetObjects.Length; i++)
                Targets[i] = TargetObjects[i].GetComponent<ITarget>();
        }
        public void Action()
        {
            CheckCurrentTartget();
        }
        private void CheckCurrentTartget()
        {
            if (_CurrentTarget == null || !_CurrentTarget.IsActive())
            {
                if (targetComparer == null )
                    Reset();
                
                targetComparer.CurrentPosition = transform.position;
                Array.Sort(Targets, targetComparer);

                for (int i = 0; i < Targets.Length; i++)
                {
                    if (Targets[i].IsActive())
                    {
                        if (_CurrentTarget != Targets[i])
                        {
                            _CurrentTarget = Targets[i];
                            CurrentTargetObject = _CurrentTarget.GetGameObject();
                            AI.SetTarget(_CurrentTarget);
                            return;
                        }
                    }
                }
            }
        }
        public override void OnSceneLoadComplete()
        {
            _MeshAnimator = GetComponentInChildren<SnapshotMeshAnimator>();
            MainManager.Instance.EventManager.Register(EventTypes.LevelStart, Run, true);
            _MeshAnimator.enabled = false; 
            Reset();
        }

        public void Run(EventArgs Args)
        {
            _MeshAnimator.enabled = true;
        }

        public void Reset()
        {
            BonusesPicked = new List<int>();
            targetComparer = new TargetComparer();
            GetTargets();
            _CurrentTarget = null;
            life = lifeMax;
            AI.Reset();
        }
        public bool Collect(ICollectable Bonus)
        {
            if(BonusesPicked == null)
                BonusesPicked = new List<int>();

            if (BonusesPicked.Contains(Bonus.GetID()))
                return false;
            else
            {
                BonusesPicked.Add(Bonus.GetID());
                return true;
            }
        }
        public void GetDamage(float damage)
        {
            life -= damage;
            if (life <= 0)
                Die();
        }

        public void Die()
        {
            isDead = true;
            gameObject.SetActive(false);
            deathParticle = MainManager.Instance.PoolingManager.GetGameObjectById(DeathParticle).GetComponent<ParticleSystem>();
            if(MainManager.Instance.GameManager.Parameters.ExtraViolence)
            { 
                headObject = MainManager.Instance.PoolingManager.GetGameObjectById(Head);
                headObject.transform.localScale *= MobScale;
                headObject.transform.position = transform.position + Vector3.up * 2;

                headObject.GetComponent<Rigidbody>().AddForce(((Vector3)UnityEngine.Random.insideUnitCircle  + Vector3.up)*10, ForceMode.Impulse);
                Delayer.Delay(3f, () => {
                    if (deathParticle.gameObject.activeInHierarchy)
                    {
                        deathParticle = MainManager.Instance.PoolingManager.GetGameObjectById(DeathParticle).GetComponent<ParticleSystem>();
                        deathParticle.gameObject.transform.position = headObject.transform.position;
                        deathParticle.transform.Rotate(Vector3.left * 90);
                        deathParticle.Play();
                    }
                        headObject.SetActive(false);
                    headObject.transform.localScale *= 1 / MobScale;
                    });
            }
            deathParticle.transform.Rotate(Vector3.left * 90);
            if (deathParticle != null)
            {
                deathParticle.transform.position = transform.position + Vector3.up;
                deathParticle.Play();
            }

            Delayer.Delay(.3f, () => { isDead = false; Reset(); });
        }
        public float GetHealt()
        {
            return life;
        }
        private void LateUpdate()
        {
            CheckCurrentTartget();
        }
        public void SetHealt(float healt){}
        public void Heal(float healt){}
        public bool IsDead(){return false;}
        public void Inheritance(IGameObject Mother)
        {
            BonusesPicked.AddRange(Mother.GetGameObject().GetComponent<MobBase>().BonusesPicked);
        }
        public float GetHealtPercent()
        {
            return (float)life / lifeMax;
        }
        public void SetPosition(Vector3 position)
        {
            AI.setPosition(position);
        }
        private void Awake()
        {
            transform.localScale *= MainManager.Instance.GameManager.Parameters.MobScale;
        }
    }

    public class TargetComparer : IComparer<ITarget>
    {
        public Vector3 CurrentPosition;
        public int Compare(ITarget x, ITarget y)
        {
           if(Math.Abs(x.GetTargetPosition().x - CurrentPosition.x) <
                Math.Abs(y.GetTargetPosition().x - CurrentPosition.x))
                return -1;
           else if (Math.Abs(x.GetTargetPosition().x - CurrentPosition.x) ==
                Math.Abs(y.GetTargetPosition().x - CurrentPosition.x))
                return 0;
            else
                return 1;
        }
        
    }
    
}
