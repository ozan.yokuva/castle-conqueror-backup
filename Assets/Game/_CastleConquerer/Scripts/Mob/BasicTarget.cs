using UnityEngine;
using EC.Managers;

namespace EC.CastleConqueror
{
    public class BasicTarget : ECMonoBehaviour, ITarget
    {
        public bool isActive = true;
        public override int _PriorityLevel => 100;

        public Vector3 _TargetOffset;
        ILife LifeHandler;
        public bool IsActive()
        {
            if (LifeHandler == null)
                return true;

         isActive = !LifeHandler.IsDead();

            return isActive;
        }

        public Vector3 GetTargetPosition()
        {
            return transform.position + _TargetOffset;
        }

        public override void OnSceneLoadComplete()
        {
        LifeHandler = GetComponent<ILife>();
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }
}
