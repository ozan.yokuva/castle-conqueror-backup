using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IParameter<T>
{
    T Value { get; set; }

}
