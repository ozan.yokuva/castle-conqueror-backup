using UnityEngine;

namespace EC.CastleConqueror
{
    public interface ITarget
    {
        Vector3 GetTargetPosition();
        GameObject GetGameObject();
        bool IsActive();
    }
}
