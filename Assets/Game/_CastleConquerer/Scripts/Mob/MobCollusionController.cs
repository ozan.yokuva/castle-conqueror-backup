using UnityEngine;
using EC.Managers;


namespace EC.CastleConqueror
{
    public class MobCollusionController : ECMonoBehaviour
    {
        public override int _PriorityLevel => 210;
        float othersLife;
        ILife _Mob, OtherMob;
        public override void OnSceneLoadComplete()
        {
            _Mob = GetComponent<ILife>();
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == 6)
            {
                if (!other.gameObject.CompareTag(tag))
                {
                        OtherMob = other.GetComponent<ILife>();
                    if (OtherMob == null)
                        OtherMob = other.GetComponentInParent<ILife>();

                    if (OtherMob != null)
                    {
                        othersLife = OtherMob.GetHealt();
                        if (_Mob != null)
                        {
                            OtherMob.GetDamage(_Mob.GetHealt());
                            _Mob.GetDamage(othersLife);
                        }
                    }
                   
                }
            }

        }
    }
}
