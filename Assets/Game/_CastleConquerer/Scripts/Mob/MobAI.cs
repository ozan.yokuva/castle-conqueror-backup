using UnityEngine;
using UnityEngine.AI;
using EC.Managers;

namespace EC.CastleConqueror
{
    public class MobAI : ECMonoBehaviour
    {
        public override int _PriorityLevel => 200;
        GameParameters _Paramaters
        {
            get {
                return MainManager.Instance.GameManager.Parameters;

            }
        }
        NavMeshAgent _Agent;
        ITarget _Target;
        Vector3 _CurrentTargetPosition;
        Vector3  Temp_Vector;
        
        public override void OnSceneLoadComplete()
        {
            _Agent = GetComponent<NavMeshAgent>();
            _Agent.enabled = true;
        }

        public void Reset()
        {
            _Target = null;
        }
        public void SetTarget(ITarget Target)
        {
            _Target = Target; 
        }

        void CalculateTargetPosition()
        {
            _CurrentTargetPosition = _Target.GetTargetPosition();
            if (Mathf.Abs((_CurrentTargetPosition - transform.position).z) > _Paramaters.MobTargetLockDistance)
                _CurrentTargetPosition.x = transform.position.x;

        }
        void move()
        {
            if (_Target != null && _Target.IsActive())
            {
                CalculateTargetPosition();

                Temp_Vector = (_CurrentTargetPosition - transform.position).normalized * 
                    _Paramaters.MobSpeed * Time.deltaTime;
                if (_Agent == null)
                    _Agent = GetComponent<NavMeshAgent>();
                _Agent.Move(Temp_Vector);
            }
        }
        public void setPosition(Vector3 pos)
        {
            if (_Agent == null)
                _Agent = GetComponent<NavMeshAgent>();

            _Agent.enabled = false;
            transform.position = pos;
            _Agent.enabled = true;

        }
        private void FixedUpdate()
        {
            if(_Paramaters.State == GameState.Play)
                    move();
        }
    }
}

