using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpawnableSource<T>
{
    ISpawnable fetch(T ID);
}
