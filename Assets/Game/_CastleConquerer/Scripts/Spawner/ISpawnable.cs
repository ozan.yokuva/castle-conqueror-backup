using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpawnable 
{
    GameObject GetGameObject();
    void StartAction();
    void SetPosition(Vector3 position);


}
