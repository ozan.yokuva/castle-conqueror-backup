using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using EC.Managers;



public class SpawnerBase : ECMonoBehaviour , ITextFieldSupplier,ILife
{
    [SerializeField] float lifeMax;
    float life;
    ISpawner _Spawner;
    [SerializeField]bool isPlayer = true;
    UIBloodEffect _BloodEffect;

    public override int _PriorityLevel =>300;
    public event Action OnDeathEvent;
    public event Action OnLifeChangeEvent;
    

    Action ILife.OnDeath { get => OnDeathEvent; set => OnDeathEvent = value; }
    Action ILife.OnLifeChanged { get => OnLifeChangeEvent ; set => OnLifeChangeEvent=value; }

    float ShakeMagnitude => MainManager.Instance.GameManager.Parameters.ScreenShakeMagnitude;
    float ShakeDuration => MainManager.Instance.GameManager.Parameters.ScreenShakeDuration;
    float HapticSpawnerDie => MainManager.Instance.GameManager.Parameters.HapticSpawnerDestroyed;
    public string GetText()
    {
        return _Spawner.GetSpawnTimer().ToString("0.0");
    }

    public void Die()
    {
        GetComponentInChildren<Collider>().enabled = false;
        Shake.Instance.ShakeIt(ShakeDuration, ShakeMagnitude);
        EC.Utility.HapticController.Instance.Vibrate((MoreMountains.NiceVibrations.HapticTypes)HapticSpawnerDie);
        OnDeathEvent?.Invoke();
        if (isPlayer&& _BloodEffect!=null)
            _BloodEffect.triggerAction();

    }

    public void GetDamage(float damage)
    {
        life -= damage;
        if (life <= 0)
            Die();
        OnLifeChangeEvent?.Invoke();

    }

    public float GetHealt()
    {
        return life;
    }



    public void Heal(float healt)
    {
        throw new System.NotImplementedException();
    }

    public bool IsDead()
    {
        return GetHealt() <= 0f;
    }
    public void Reset()
    {
        life = lifeMax;
    }
    public void SetHealt(float healt)
    {
        life = healt;
    }

    public override void OnSceneLoadComplete()
    {
        _Spawner = GetComponent<ISpawner>();
        SetHealt(lifeMax);
        _BloodEffect = FindObjectOfType<UIBloodEffect>();


    }

    public float GetHealtPercent()
    {
        return (float) life / lifeMax;
    }
}
