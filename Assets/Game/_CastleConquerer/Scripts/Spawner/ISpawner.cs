using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpawner 
{
    float GetSpawnTimer();
    void Spawn();
    Vector3 GetSpawnPoint();


}
