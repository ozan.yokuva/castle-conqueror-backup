using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EC.Managers;

namespace EC.CastleConqueror
{
    public class SpawnablePoolBin : ECMonoBehaviour, ISpawnableSource<PoolID>
    {
        GameObject _TempGameObject;
        

        public override int _PriorityLevel => 50;
        public ISpawnable fetch(PoolID ID)
        {
            _TempGameObject =  MainManager.Instance.PoolingManager.GetGameObjectById((PoolID)ID);
             return _TempGameObject.GetComponent<ISpawnable>();
        }
        public override void OnSceneLoadComplete()
        {
        
        }
    }
}
