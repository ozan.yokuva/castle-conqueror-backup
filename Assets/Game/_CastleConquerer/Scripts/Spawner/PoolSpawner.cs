using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EC.Managers;
using EC.Utility;

namespace EC.CastleConqueror
{
    public class PoolSpawner : ECMonoBehaviour, ISpawner
    {

        GameParameters _Paramaters
        {
            get
            {
                return MainManager.Instance.GameManager.Parameters;

            }
        }

        public override int _PriorityLevel => 105;
        ISpawnableSource<PoolID> _SpawnableSource;
        ISpawnable _Spawn;
        public PoolID _SpawnObjectID;
        float _LastSpawn;
        public int SpawnSize;
        [SerializeField] float _SpawnDelay = 5;
        [SerializeField] Vector3 _SpawnPointOffset = Vector3.forward;
        ILife LifeHandler;
        float _TimeLeftToNextSpawn {
            get
            {

                return Mathf.Clamp((_LastSpawn + _SpawnDelay) - Time.time, 0, _SpawnDelay);
            }
        }



        public Vector3 GetSpawnPoint()
        {
            return transform.position + _SpawnPointOffset;
        }

        public float GetSpawnTimer()
        {
            return _TimeLeftToNextSpawn;
        }

        public void Spawn()
        {
            for (int i = 0; i < SpawnSize; i++)
            {
                _Spawn = _SpawnableSource.fetch(_SpawnObjectID);
                Vector3 randomVector = (Vector3)Random.insideUnitCircle;

                Vector3 generatedPosition = GetSpawnPoint();

                generatedPosition.x += randomVector.x;
                generatedPosition.z += randomVector.y;



                // Debug.Log( i + ":" + generatedPosition);
                
                _Spawn.SetPosition(generatedPosition);
                _Spawn.StartAction();
             
                
                _LastSpawn = Time.time;
            }
        }

        public void CheckSpawnTimer()
        {
            if (_TimeLeftToNextSpawn == 0 && !LifeHandler.IsDead())
                Spawn();
        }
        private void LateUpdate()
        {
            if (_Paramaters.State == GameState.Play)
                CheckSpawnTimer();
        }

        public override void OnSceneLoadComplete()
        {
            _LastSpawn = -_SpawnDelay;
            _SpawnableSource = GetComponent<ISpawnableSource<PoolID>>();
            LifeHandler = GetComponent<ILife>();
            Spawn();
        }
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(GetSpawnPoint(), 1);
        }
    }
}
