using EC.Managers;
using System;
using UnityEngine;


namespace EC.ShufflePeople
{
    public class CurvativeMovement : ECMonoBehaviour
    {
        [SerializeField] Axis _Axis;
        [SerializeField] float _Multiplier;
        [SerializeField] float _Speed;
        [SerializeField] AnimationCurve _MotionCurve;
        Vector3 _TempVector,_OriginalPosition,_DirectionVector;
        bool initialized = false;

        public override int _PriorityLevel =>100 ;

        

        public override void OnSceneLoadComplete()
        { 
            _OriginalPosition = transform.localPosition;
            initialized = true;

        }

        void FixedUpdate()
        {
            if (initialized)
            {
                switch (_Axis)
                {
                    case Axis.x:
                        _DirectionVector = Vector3.right;
                        break;
                    case Axis.y:
                        _DirectionVector = Vector3.up;
                        break;
                    case Axis.z:
                        _DirectionVector = Vector3.forward;
                        break;
                    default:
                        break;
                }

                transform.localPosition = _OriginalPosition + _DirectionVector *
                    _MotionCurve.Evaluate((_Speed * Time.time) % 1) * _Multiplier;
            }
        }
    }

    public enum Axis
    { 
        x,y,z
    
    }
}
