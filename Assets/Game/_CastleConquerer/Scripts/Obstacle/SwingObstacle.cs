using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwingObstacle : MonoBehaviour
{

    [SerializeField] AnimationCurve _SwingBehaviour;
    [SerializeField] float _Speed = 1;
    [SerializeField] float _Degree = 45;
    [SerializeField] Transform _SwingTransform;

    Quaternion _OriginalRotation;
    float _CurrentDegree;
    private void Awake()
    {
        _OriginalRotation = _SwingTransform.localRotation;
    }

    void Swing()
    {
        _CurrentDegree = _SwingBehaviour.Evaluate((_Speed* Time.time)  % 1)* _Degree;
        _SwingTransform.transform.localRotation = Quaternion.Euler(0, _CurrentDegree, 0);

        
    
    }
    private void LateUpdate()
    {
        Swing();
    }
    // Update is called once per frame

}
