using System;
using System.Collections.Generic;
using UnityEngine;

namespace EC.Managers
{
    public abstract class ECMonoBehaviour : MonoBehaviour
    {

        //Pool sonradan initialize edemiyor 
        //is initialize complete

        private static ECMonoBehaviour[] SceneMonos;
        public abstract  int _PriorityLevel {
            get;
        }
        private static bool LoggingOn = false;
        
        public abstract void OnSceneLoadComplete();

        public static void Initialize()
        {
            SceneMonos = GameObject.FindObjectsOfType<ECMonoBehaviour>(true);

            Array.Sort(SceneMonos, new ECMonoComparetor());

            for (int i = 0; i < SceneMonos.Length; i++)
            {
              /*  if (LoggingOn)
                    Debug.Log("Initializing :... " + SceneMonos[i]+ "-Priority:" + SceneMonos[i]._PriorityLevel);*/
                SceneMonos[i].OnSceneLoadComplete();
            }
        }
        public static void setLog(bool logOn)
        {
            LoggingOn = logOn;
        }
    }
    public class ECMonoComparetor : IComparer<ECMonoBehaviour>
    {
        public int Compare(ECMonoBehaviour x, ECMonoBehaviour y)
        {
            if (x._PriorityLevel > y._PriorityLevel)
                return 1;
            else if (x._PriorityLevel < y._PriorityLevel)
                return -1;
            else
                return 0;
        }
    }
}