﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EC.CastleConqueror
{
    public class SoloCamShake : MonoBehaviour
    {
        GameParameters _Parameters
        {
            get
            {
                return EC.Managers.MainManager.Instance.GameManager.Parameters;
            }
        }
        public Vector3 currentPos;
        void LateUpdate()
        {
            transform.position = currentPos + Shake.Instance.getCurrentOffset();
        }

        private void Awake()
        {
            currentPos = transform.position;
        }
    }
   
}
