
using UnityEngine;
using UnityEngine.Events;
using EC.Utility;
using EC.Managers;
using TMPro;

namespace EC.CastleConqueror
{
    public class ProjectileBasic : ECMonoBehaviour, IProjectile
    {

        public float finalY;
        public override int _PriorityLevel => 150;

        [SerializeField] int _ActivateOnCount =>MainManager.Instance.GameManager.Parameters.ProjectileActivateCollusionCount;
        [SerializeField] float _ActivateOnTime => MainManager.Instance.GameManager.Parameters.ProjectileActivateMinDuration;
        [SerializeField] int _HapticCollusion => MainManager.Instance.GameManager.Parameters.HapticProjectiteCollusion;
        [SerializeField] UnityEvent _OnFire;
        [SerializeField] UnityEvent _OnSet;
        [SerializeField] TextMeshPro _Text;
        
        ICollectable _Collectable;
        Rigidbody _Rigidbody;
        Vector3 _TempVector;
        bool _Fired;
        int _ProjectileID;
        int _CollusionCount;
        public void Fire()
        {
           _OnFire?.Invoke();
            _Fired = true;
        }
        public GameObject GetGameObject()
        {
            return gameObject;
        }
        public override void OnSceneLoadComplete()
        {
            _Rigidbody = GetComponent<Rigidbody>();
        }

        private void OnCollisionEnter(Collision collision)
        {


            Expand();

        }


        public void Expand()
        {
            if (_Fired == true)
            {
                Delayer.Delay(_ActivateOnTime, Expand);
                _CollusionCount++;
                if (_CollusionCount >= _ActivateOnCount)
                {
                    doPostEffect();
                }
            }

        }

        void doPostEffect()
        {
            HapticController.Instance.Vibrate((MoreMountains.NiceVibrations.HapticTypes)_HapticCollusion);
            gameObject.SetActive(false);
            _Fired = false;
            _Rigidbody.isKinematic = true;
            _Rigidbody.useGravity = false;
            _Collectable.StartAction();
            _TempVector = transform.position;
            _TempVector.y = finalY;
            _Collectable.GetGameObject().transform.position = _TempVector;
        }

        public void SetID(int ID)
        {
            _ProjectileID = ID;
            int retryCount = 5;

            try
            {
                _Collectable = CollectableSource.Instance.FetchRandomCollectable();
            }
            catch
            {
                Debug.Log("Could not read a collectable , Retry:" + retryCount);
            }

            while (retryCount > 0 && _Collectable ==null)
            {
                try { _Collectable = CollectableSource.Instance.FetchRandomCollectable(); }
                catch
                {
                    Debug.Log("Could not read a collectable , Retry:"+ retryCount);
                }
            }



            GetComponentInChildren<MeshRenderer>().material = _Collectable.GetMaterial();
            _Text.text = _Collectable.GetText();
            _Collectable.SetID(_ProjectileID);
            _OnSet?.Invoke();
            _CollusionCount =0;

        }
    }
}
