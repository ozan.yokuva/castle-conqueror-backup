using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public interface IProjectile 
{
    GameObject GetGameObject();

    void SetID(int ID);
    void Fire();

}
