using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EC.Managers;

namespace EC.CastleConqueror
{
    public class CollectableSource : ECMonoBehaviour
    {
        public static CollectableSource Instance;
        
        [SerializeField] List<CollectableType> Collectables;
        float PickRandom = 0;

        public override int _PriorityLevel => 5;

        void prepareProbabilityMap()
        {
            float ProbabilitySum = 0;
            for (int i = 0; i < Collectables.Count; i++)
                ProbabilitySum += Collectables[i].Probability;
            for (int i = 0; i < Collectables.Count; i++)
                Collectables[i].actualProbability = Collectables[i].Probability * (100f / ProbabilitySum);

        }

        private void OnEnable()
        {
            Instance = this;
            prepareProbabilityMap();
        }

        private CollectableType getRandomCollectableType()
        {
            PickRandom = Random.Range(0f, 100f);

            int findIndex = 0;
            while (PickRandom > 0)
            {
                if (PickRandom < Collectables[findIndex].actualProbability)
                {
                    return Collectables[findIndex];
                }
                else
                    PickRandom -= Collectables[findIndex].actualProbability;

                findIndex++;

            }
            
            return Collectables[0];
        }

        public ICollectable FetchRandomCollectable()
        {
            CollectableType TType = getRandomCollectableType();

            ICollectable TCollectable = EC.Managers.MainManager.Instance.PoolingManager.GetGameObjectById(TType.CollectableId).GetComponent<ICollectable>();
            TCollectable.GetGameObject().transform.position = Vector3.one * 999;
            TCollectable.SetActiveValue(TType.ActiveValue);
            return TCollectable;


        }

        public override void OnSceneLoadComplete()
        {
            Instance = this;
            prepareProbabilityMap();
        }
    }


    [System.Serializable]
    public class CollectableType
    {
        public PoolID CollectableId;
        public float ActiveValue;
        [Range(0, 100)] public float Probability;
        public float actualProbability;
    }

}
