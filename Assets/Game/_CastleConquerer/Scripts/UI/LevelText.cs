using EC.Managers;
using System.Collections;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelText : ECMonoBehaviour
{
    [SerializeField] Text _Text;

    public override int _PriorityLevel =>0;

    public override void OnSceneLoadComplete()
    {
        _Text.text = "Level " + MainManager.Instance.GameManager.GetPlayerLevel();
    }


}
