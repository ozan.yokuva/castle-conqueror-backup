using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITextFieldSupplier 
{
    string GetText();

}
