using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIUpdater : MonoBehaviour
{

    [SerializeField] TextMeshPro _TextToUpdate;
    [SerializeField] GameObject _SupplierObject;
    ITextFieldSupplier _Supplier;

    private void Awake()
    {
        _Supplier = _SupplierObject.GetComponent<ITextFieldSupplier>();

    }

    private void LateUpdate()
    {
        if (_Supplier != null)
            _TextToUpdate.text = _Supplier.GetText();
    }
}
