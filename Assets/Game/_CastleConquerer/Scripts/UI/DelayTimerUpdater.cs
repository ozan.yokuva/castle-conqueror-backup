using UnityEngine;
using TMPro;
using UnityEngine.UI;
using EC.Utility;


namespace EC.CastleConqueror
{
    public class DelayTimerUpdater : MonoBehaviour
    {
        [SerializeField]PlayerProjectileManager _ProjectileManager;
        [SerializeField]TextMeshProUGUI _TimeLeftText;
        [SerializeField]Image _FrameImage;
        [SerializeField]Image _TimerImage;



        private void UpdateData()
        {
            if (_TimerImage != null)
            {
                float timeLeft = _ProjectileManager.getTimeLeftToNext();
                _TimerImage.enabled = timeLeft > 0;
                _FrameImage.enabled = timeLeft > 0;
                _TimeLeftText.enabled = timeLeft > 0;
                _TimeLeftText.text = timeLeft.ToString("0.0");
                _TimerImage.fillAmount = 1 - _ProjectileManager.getTimeLeftpercent();
                Delayer.Delay(0.1f, UpdateData);
            }
        }
        // Update is called once per frame
        private void Start()
        {
            UpdateData();
            
        }
    }
}
