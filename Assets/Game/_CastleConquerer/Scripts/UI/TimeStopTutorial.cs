using EC.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EC.CastleConqueror;

public class TimeStopTutorial : ECMonoBehaviour
{

    GameParameters _Parameters => MainManager.Instance.GameManager.Parameters;
    [SerializeField]Canvas canvas;
    Vector3 OriginalScale;
    Image image;
    public override int _PriorityLevel =>100;
    private void Awake()
    {
        OriginalScale = transform.localScale;
    }

    public override void OnSceneLoadComplete()
    {
        MainManager.Instance.EventManager.Register(EventTypes.LevelStart, stooop, true);
    }
    void stooop(EventArgs args)
    {
        _Parameters.State = GameState.First;
        canvas.enabled = true;
        Time.timeScale = 0;
    }
   

    public void Release()
    {
        canvas.enabled = false;
        _Parameters.State = GameState.Play;
        Time.timeScale = 1;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
