using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using EC.Managers;

public class UILifeBarUpdater : ECMonoBehaviour
{
    ILife LifeHandler;
    ILife _LifeHandler
    {
        get { 
        
            if(LifeHandler==null)
                LifeHandler = GetComponent<ILife>();
        return LifeHandler;

        }
    }
    public override int _PriorityLevel => 120;
    [SerializeField]Image _Image;
    private void UpdateLife()
    {

        if (_Image != null)
            _Image.fillAmount = LifeHandler.GetHealtPercent();
    }


    private void OnDestroy()
    {
        if(LifeHandler!=null)
        LifeHandler.OnLifeChanged -= UpdateLife;
    }

    public override void OnSceneLoadComplete()
    {
        LifeHandler = GetComponent<ILife>();
        LifeHandler.OnLifeChanged += UpdateLife;
      
    }
}
