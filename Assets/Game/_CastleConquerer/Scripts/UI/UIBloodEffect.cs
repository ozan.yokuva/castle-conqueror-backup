using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EC.Utility;

public class UIBloodEffect : MonoBehaviour
{
    [SerializeField] float _Speed;
    [SerializeField] Image _Image;
    public Color _Color;
    bool _OnView = false;

    public void triggerAction()
    {
        _OnView = true;
        _Color = _Image.color;
        _Color.a = .6f;
        progress();        
    }

    public void progress()
    {
        _Color.a = _Color.a - 0.01f * _Speed; 
        _Image.color = _Color;
        if (_Color.a > 0)
            Delayer.Delay(Time.deltaTime, progress);
    }
    
}
